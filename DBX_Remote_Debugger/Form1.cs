﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Renci.SshNet;

namespace DBX_Remote_Debugger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var connectionInfo = new ConnectionInfo("localhost", 2222, "marcinl", new PasswordAuthenticationMethod("marcinl", "Haslo123"));
            var client = new SshClient(connectionInfo);
            client.Connect();
            if(client.IsConnected)
            {
                MessageBox.Show("Connection established");
            }
        }
    }
}
